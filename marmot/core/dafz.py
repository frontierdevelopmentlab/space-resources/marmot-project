"""
DAFZ Multi-Agent Planner for Moon Prospecting.

This is a modification of the DCPT algorithm presented by Subhrajit
Bhattacharya, Maxim Likhachev, and Vijay Kumar.

The paper is located here:
https://www.cs.cmu.edu/~maxim/files/DPCT_icra10.pdf

Author: Zahi Kakish (GitHub/GitLab: zmk5)

"""
import numpy as np
import networkx as nx


def unconstrained_cost_function(current_node, next_node, graph):
    """ Cost Function for DPCT Algorithm """
    x_val, y_val = current_node
    return graph[x_val, y_val][next_node]["weight"]


def sum_penalties(r_t, iter_val):
    """
    Sum penalties of each robot's path.

    Sums the penalties of all the paths at an iteration. Return value is used
    as a conditional for the while loop of the DPC algorithm.

    Parameters
    ----------
    r_t : RobotTeam()
        Object containing information about all the robots, which will be used
        to iterate through.
    iter_val : int
        The iteration value of the algorithm.

    Returns
    -------
    total : int or float
        Sum of all the penalties between every pair of robots.

    """
    total = 0
    robot_id = 1
    for j in range(2, r_t.num_of_robots + 1):
        if j != robot_id:
            for ind in range(0, len(r_t.robot[robot_id].path[iter_val])):
                state = r_t.robot[robot_id].path[iter_val][ind]["s"]
                time = r_t.robot[robot_id].path[iter_val][ind]["t"]
                total += penalty_function(
                    time, ind, state, r_t.robot[j].path[iter_val])

        robot_id += 1
        if robot_id >= r_t.num_of_robots:
            robot_id = 1

    return total


def sum_penalties_with_weights(r_t, iter_val, weights):
    """
    Sum penalties of each robot's path with their respective weights.

    Sums the penalties of all the paths at an iteration with weights for each
    robot pair used in the DPC algorithm. Return value is used as a conditional
    for the while loop of the DPC algorithm.

    Parameters
    ----------
    r_t : RobotTeam()
        Object containing information about all the robots, which will be used
        to iterate through.
    iter_val : int
        The iteration value of the algorithm.

    Returns
    -------
    total : list of int or float
        List of sums for all the penalties between every pair of robots.

    """
    total = []
    temp_total = 0
    robot_id = 1
    for j in range(2, r_t.num_of_robots + 1):
        if j != robot_id:
            for ind in range(0, len(r_t.robot[robot_id].path[iter_val])):
                state = r_t.robot[robot_id].path[iter_val][ind]["s"]
                time = r_t.robot[robot_id].path[iter_val][ind]["t"]
                temp_total += penalty_function(
                    time, ind, state, r_t.robot[j].path[iter_val])

        # Append product of penalty sum with weight for robot i, j, where i
        # is `robot_id' and j is `j`. -1 for zero index.
        total.append(weights[iter_val][robot_id - 1, j - 1] * temp_total)

        robot_id += 1
        if robot_id >= r_t.num_of_robots:
            robot_id = 1

    return total


def penalty_function(time, index_1, robot_1_state, robot_2_path, phi=50):
    """ Penalty Function for DPCT Algorithm """
    index_2 = time_lookup(time, robot_2_path, index=index_1, search_all=True)
    return dist_function(robot_1_state,
                         robot_2_path[index_2]["s"]) - phi


def dist_function(node_1, node_2):
    """ Simple Euclidean Distance Function """
    return np.sqrt(np.power(node_2[0] - node_1[0], 2) +
                   np.power(node_2[1] - node_1[1], 2))


def heuristic_function(next_node, goal_node):
    """ Simple Heuristic Function for DPCT Algorithm """
    return dist_function(next_node, goal_node)


def time_lookup(time, robot_pd, index=None, bias=5, search_all=False):
    """
    Look up data associated with a time during a robots path.

    The function looks up a certain timestep within a robots path. If it is
    within the robot path, the function will return the index that time is
    located in the robot path. If not, the function will return the index of
    the last timestep in the robot path.

    The first argument, `index`, is the index of a different robot's path data
    time value, which we are looking up for in another robot's path data. This
    can be a speed up in lookup times if time values between multiple robots
    are consistent.

    Parameters
    ----------
    time : float or int
        Current time to look-up in the other robot path.
    robot_pd : dict
        The robot's path where to look up the robot's state associated with
        the time argument.
    index : int
        Current path index of the original robot agent.
    bias : int (default=5)
        To speed up look-up, the index of the original agent is provided. This
        index is biased in cas the original index is in the past a few steps.
    search_all : bool (default=False)
        Instead of starting at a certain index to speed up the time lookup,
        the following option allows the user to search throughout the whole
        path of the agent. This will increase computational time, but will be
        more accurate.

    Returns
    -------
    i_val : int
        The index within the robot_path that matches the time value provided
        in the argument.

    """
    i_val = 0
    # Search all data points
    if search_all:
        for i in range(0, len(robot_pd) - 1):
            if robot_pd[i]["t"] <= time <= robot_pd[i + 1]["t"]:
                i_val = i + 1
                break

    # Else apply bias if possible for speedup
    else:
        try:
            for i in range(index - bias, len(robot_pd) - 1):
                if robot_pd[i]["t"] <= time <= robot_pd[i + 1]["t"]:
                    i_val = i + 1
                    break

        except KeyError:
            for i in range(index, len(robot_pd) - 1):
                if robot_pd[i]["t"] <= time <= robot_pd[i + 1]["t"]:
                    i_val = i + 1
                    break

    # Max data point index if a time range doesn't exit or not found.
    if i_val == 0 and index > 10:
        i_val = len(robot_pd) - 1

    return i_val


def unconstrained_search_function(start, goal, graph):
    """
    Unconstrained A* Search

    A* search that just calculates cost based on the weights on the edges of
    the cost map and a simple heuristic function.

    Parameters
    ----------
    start : tuple
        Index indicating the start location of the robot.
    goal : tuple
        Index indicating the goal location of the robot.
    graph : nx.classes.graph.Graph
        Directed graph containing all the possible locations for the robot to
        travel.

    Returns
    -------
    path : list
        This dataset contains the state, s, time, t, task, B, and cost, c,
        for each part of the path (integer values start from 0 are the keys).

    """
    current_node = start  # current
    next_node = None  # next
    path = {
        0: {
            "s": start,  # (x, y)
            "t": 0,  # time at position
            "B": 0,  # task
            "c": 0,},  # cost
    }
    previous_path_points = [start]
    queue = {}
    i = 0

    # Try statement to provide a backup in case a low-cost path is uanble
    # to be found.
    try:
        while True:
            # Get next set of edges for the node.
            next_possible_nodes = []
            queue[current_node] = {}

            # Get list of possible nodes to go to
            for key in graph[current_node[0], current_node[1]]:
                if isinstance(key, tuple) and key not in previous_path_points:
                    next_possible_nodes.append(key)

            # Begin search on the list
            for next_node in next_possible_nodes:
                # Get cost and add to queue
                next_cost = path[i]["c"] + unconstrained_cost_function(
                    current_node, next_node, graph)
                queue[current_node][next_node] = next_cost + \
                    heuristic_function(next_node, goal)

            # Pick lowest cost as the next node:
            next_best = min(queue[current_node].keys(),
                            key=(lambda k: queue[current_node][k]))

            # Set values for variables and check if goal reached.
            path[i + 1] = {"s": next_best,
                           "t": path[i]["t"] + graph[next_best[0],
                                                     next_best[1]]["t"],
                           "B": 0,
                           "c": min(queue[current_node].values())}
            previous_path_points.append(next_best)
            current_node = next_best
            if current_node == goal:
                break

            # increment index
            i += 1

    except ValueError:
        print("No path found!")

    return path


def constrained_search_function(start, goal, graph, robot_team, weights,
                                robot_id, iter_val):
    """
    Constrained A* Search

    A* search that just calculates cost based on the weights on the edges of
    the cost map, a simple heuristic function, and the soft constraints used in
    the DPC algorithm.

    Parameters
    ----------
    start : tuple
        Index indicating the start location of the robot.
    goal : tuple
        Index indicating the goal location of the robot.
    graph : nx.classes.graph.Graph
        Directed graph containing all the possible locations for the robot to
        travel.
    robot_team : RobotTeam
        Object containing the information of multiple robot agents, associated
        roles, and individual agent data.
    robot_id : int
        The id for the robot for whom the search function is applied.
    iter_val : int
        The iteration of the dafz algorithm.

    Returns
    -------
    path : dict
        This dataset contains the state, s, time, t, task, B, and cost, c,
        for each part of the path (integer values start from 0 are the keys).

    """
    current_node = start  # current
    next_node = None  # next
    path = {
        0: {
            "s": start,
            "t": 0,  # time at position
            "B": 0,  # task
            "c": 0,},  # cost
    }
    previous_path_points = [start]
    queue = {}
    i = 0

    # Try statement to provide a backup in case a low-cost path is uanble
    # to be found.
    try:
        while True:
            # Get next set of edges for the node.
            next_possible_nodes = []
            queue[current_node] = {}

            # Get list of possible nodes to go to
            for key in graph[current_node[0], current_node[1]]:
                if isinstance(key, tuple) and key not in previous_path_points:
                    next_possible_nodes.append(key)

            # Begin search on the list
            for next_node in next_possible_nodes:
                # Sum penalties for all agents at that timestep.
                penalty = 0
                for j in range(1, robot_team.num_of_robots + 1):
                    if robot_id != j:
                        t_val = path[i]["t"] + \
                            graph[next_node[0], next_node[1]]["t"]
                        penalty += weights[iter_val][robot_id - 1, j - 1] * \
                            penalty_function(t_val, i, next_node,
                                             robot_team.robot[j].path[iter_val])

                # Get cost and add to queue
                next_cost = path[i]["c"] + \
                            unconstrained_cost_function(
                                current_node, next_node, graph) + penalty
                queue[current_node][next_node] = next_cost + \
                    heuristic_function(next_node, goal)

            # Pick lowest cost as the next node:
            # print(iter_val, i, queue[current_node])
            next_best = min(queue[current_node].keys(),
                            key=(lambda k: queue[current_node][k]))
            # print(queue[current_node][next_best])
            # print(robot_id, iter_val, path[i])

            # Set values for variables and check if goal reached.
            path[i + 1] = {"s": next_best,
                           "t": path[i]["t"] + graph[next_best[0],
                                                     next_best[1]]["t"],
                           "B": 0,
                           "c": min(queue[current_node].values())}
            previous_path_points.append(current_node)
            current_node = next_best
            if current_node == goal:
                break

            # increment index
            i += 1

    except ValueError:
        print(iter_val, "No optimal path found!")

    return path


def combinations(row, col):
    """ Returns iterable with tuples of all indices of a NxM matrix """
    for i in range(0, row):
        for j in range(0, col):
            yield (i, j)


def create_robot_graph(cost_map=None, number_of_nodes=100):
    """ Robot graph containing state, time, and task. """
    robot_vel = 0.01  # m/s
    tile_resolution = 60  # m
    basic_movement_time = (1 / (robot_vel * (1 / tile_resolution))) / 60
    tasks = {
        "0": int("00000000", 2),
        "10": int("00000010", 2),
        "20": int("00000100", 2),
        "30": int("00001000", 2),
        "40": int("00010000", 2),
        "50": int("00100000", 2),
        "60": int("01000000", 2),
        "70": int("10000000", 2),
    }

    task_times = {
        "movement": basic_movement_time,
        "idle-safe": 0,
        "idle-stationary": None,
        "recharge": None,
        "initial-checkout": 0,
        "NSS-calib": 0,
        "NIRVSS-calib": 0,
        "LAVA-calib": 0,
        "OVEN-calib": 0,
        "prospecting": None,
        "prospecting-DOC-image": 0,
        "AIM-prospecting-4x8": 40,
        "AIM-prospecting-DOC-image": 0,
        "AIM-prospecting-DOC-priority-image": 0,
        "panorama": 4.3,
        "drill-location-targeting": 38,
        "drilling-preparation": 20.5,
        "subsurface-assay-10-cm": 13,
        "subsurface-assay-50-cm": 65,
        "subsurface-assay-90-cm": 117,
        "subsurface-assay-100-cm": 130,
        "stow-drill": 10,
        "preheat-LAVA": 60,
        "deep-subsurface-assay-90-cm-preheat-LAVA": 117,
        "sample-collection-and-transfer": 20,
        "VA-sample-heat-up-ambient-to-150C": 60,
        "VA-sample-heat-up-150C-to-350C": 30,
        "VA-sample-heat-up-350C-to-450C": 30,
        "VA-GC-Analysis": 20,
        "water-droplet-demo": 40,
        "sample-dump": 5,
    }

    power_usage = {
        "movement": 397,
        "idle-safe": 233,
        "idle-stationary": 242,
        "recharge": 242,
        "initial-checkout": 494,
        "NSS-calib": 242,
        "NIRVSS-calib": 272,
        "LAVA-calib": 239,
        "OVEN-calib": 239,
        "prospecting": 433,
        "prospecting-DOC-image": 336,
        "AIM-prospecting-4x8": 433,
        "AIM-prospecting-DOC-image": 336,
        "AIM-prospecting-DOC-priority-image": 336,
        "panorama": 424,
        "drill-location-targeting": 433,
        "drilling-preparation": 416,
        "subsurface-assay-10-cm": 416,
        "subsurface-assay-50-cm": 416,
        "subsurface-assay-90-cm": 416,
        "subsurface-assay-100-cm": 416,
        "stow-drill": 416,
        "preheat-LAVA": 575,
        "deep-subsurface-assay-90-cm-preheat-LAVA": 511,
        "sample-collection-and-transfer": 535,
        "VA-sample-heat-up-ambient-to-150C": 670,
        "VA-sample-heat-up-150C-to-350C": 670,
        "VA-sample-heat-up-350C-to-450C": 670,
        "VA-GC-Analysis": 442,
        "water-droplet-demo": 570,
        "sample-dump": 333,
    }

    total_task_times = {
        "0": task_times["movement"],
        "1": 2,
        "2": 3,
        "3": 4,
        "4": 5,
        "5": 6,
        "6": 7,
        "7": 8,
    }

    # Create graph
    if cost_map is None:
        row = int(np.sqrt(number_of_nodes))
        col = int(np.sqrt(number_of_nodes))

    else:
        row = cost_map.shape[0]
        col = cost_map.shape[1]

    graph = nx.grid_graph(dim=[col, row])
    graph.to_directed()

    if cost_map is None:
        for i, j in combinations(row, col):
            # Add edge weights
            for key in graph[i, j]:
                if isinstance(key, tuple):
                    # graph[i, j][key]["weight"] = np.random.random_integers(
                    #     1, 10) * 1.25
                    graph[i, j][key]["weight"] = 1

            # Assign time it takes to move to vertex.
            graph[i, j]["t"] = 100  # minutes

            # Add task states and time.
            rand_list = [10, 20, 30, 40, 50, 60, 70]
            if i in rand_list and j in rand_list:
                graph[i, j]["B"] = tasks[str(i)]
                # graph[i, j]["t"] = task_times[str(i)]
                graph[i, j]["t"] = task_times["panorama"]

            else:
                graph[i, j]["B"] = tasks["0"]
                graph[i, j]["t"] = task_times["movement"]

    else:
        for i, j in range(col, row):
            # Add edge weights
            for key in graph[i, j]:
                if isinstance(key, tuple):
                    # graph[i, j][key]["weight"] = np.random.random_integers(
                    #     1, 10) * 1.25
                    graph[i, j][key]["weight"] = cost_map[j, i]

            # Assign time it takes to move to vertex.
            graph[i, j]["t"] = task_times["movement"]  # minutes

    return graph


def dpc_algorithm(r_t, graph, options=None):
    """
    Distributed Path Consensus Algorithm.

    Explanation Text. The path data for each robot within the RobotTeam()
    object class will be modified and the optimal path added.

    Paramters
    ---------
    r_t : RobotTeam()
        pass
    graph : nx.classes.DiGraph() or nx.classes.Graph()
        pass
    options : DPCOptions()
        pass

    Example
    -------
    >>> opts = DPCOptions(num_of_robots=2, max_path_cost=2000, epsilon=0.2)
    >>> opts.start = {1: (0, 0), 2: (0, 30)}
    >>> opts.goal = {1: (99, 25), 2: (88, 99)}
    >>> dpc_algorithm(r_t, graph, options=opts)

    """

    # Check if options are loaded
    if options is None:
        error = "DPCOptions object must be used for the DPC to work."
        raise ValueError(error)

    for i in [attrib for attrib in dir(options) \
            if not attrib.startswith('__') and \
            not callable(getattr(options, attrib))]:
        if options.__getattribute__(i) is None:
            raise ValueError("Option `" + i + "` not provided.")

    # Initial variables
    weights = {}  # dict for iter
    weights[0] = np.zeros(
        (options.num_of_robots, options.num_of_robots))  # i, j.
    time = 0
    iter_val = 0
    robot_id = 1

    # Find lowest cost path.
    for i in range(1, options.num_of_robots + 1):
        r_t.robot[i].path[0] = unconstrained_search_function(
            options.start[i], options.goal[i], graph)

    # Find initial sum_penalties
    penalties_sum = sum_penalties(r_t, iter_val)
    penalties_sum_with_weights = sum_penalties_with_weights(
        r_t, iter_val, weights)

    while iter_val < 3:
    # while penalties_sum != 0 and \
    #       min(penalties_sum_with_weights) <= 2 * options.max_path_cost:
        weights[iter_val + 1] = weights[iter_val] + options.epsilon

        # Compute the argmin for the robot using the cost of the path
        # and the sum of the weights and the penalties.:
        r_t.robot[robot_id].path[iter_val + 1] = \
            constrained_search_function(
                options.start[robot_id], options.goal[robot_id], graph, r_t,
                weights, robot_id, iter_val)

        # Set the path for all other robots except the current robot.
        for j in range(1, options.num_of_robots + 1):
            if j != robot_id:
                r_t.robot[j].path[iter_val + 1] = r_t.robot[j].path[iter_val]

        # Increment values
        iter_val += 1
        robot_id += 1
        time += 1

        # Reset robot number if end reached.
        if robot_id > options.num_of_robots:
            robot_id = 1

        # Find next sum_penalties
        penalties_sum = sum_penalties(r_t, iter_val)
        penalties_sum_with_weights = sum_penalties_with_weights(
            r_t, iter_val, weights)


class DPCOptions():
    """
    Options for use in the DPC algorithm.

    These parameters are required for running the DPC algorithm. Three of the
    five options are required by DPC algorithm can be given as Paramters when
    instantiating. The `start` and `end` parameters are required for manual
    entry and cannot be given a parameters to the object.

    Parameters
    ----------
    num_of_robots : int
        The number of robots within the team.
    max_path_cost : int
        The maximum cost for a path to be. Used as a stop for the progression
        of iterations within the DPC algorithm.
    epsilon : float
        The weights used for application of soft constraints are increased at
        every iteration by this value.
    start : dict (key: int, value: tuple)
        The start positions of each robot agent on the graph. Keys must be
        int values pertaining to the robot id. Values must be size-2 tuple of
        both ints or both floats, which represent x and y coordinates.
    goal : dict (key: int, value: tuple)
        The goal position of each robot agent on the graph. Keys must be
        int values pertaining to the robot id. Values must be size-2 tuple of
        bot ints or both floats, which represent x and y coordinate.

    """
    def __init__(self, num_of_robots=None, max_path_cost=None, epsilon=None):
        """ Set default options, if provided. """
        self.num_of_robots = num_of_robots
        self.max_path_cost = max_path_cost
        self.epsilon = epsilon
        self.start = None
        self.goal = None

    def _check_value(self, value):
        """ Check if start and goal arguments are of correct type. """
        assert isinstance(value, dict)
        if len(value) != self.num_of_robots:
            error = "Dict arg length must be saem as number of robots."
            raise ValueError(error)

        for key in value:
            assert isinstance(key, int)
            assert isinstance(value[key], tuple)

    @property
    def start(self):
        """ Return dict containing each robot start point (tuple). """
        return self.start

    @start.setter
    def start(self, value):
        """ Set dict containing each robot start point (tuple). """
        # Check if argument is the proper type.
        self._check_value(value)

        self.start = value

    @property
    def goal(self):
        """ Return dict containing each robot goal point (tuple). """
        return self.goal

    @goal.setter
    def goal(self, value):
        """ Set dict containing each robot goal point (tuple). """
        # Check if argument is the proper type.
        self._check_value(value)

        self.goal = value
