"""
Task Primitives for an Agent

Author: Zahi Kakish (GitHub/GitLab: zmk5)

"""


class Primitive():
    """
    Task Primitive Class
    """
    def __init__(self, name, cost, duration):
        self.name = name if isinstance(name, str) else None
        self.cost = cost if isinstance(cost, (int, float)) else None
        self.duration = duration if isinstance(duration, (int, float)) else None
        self.connection = {
            "to": {},
            "from": {},
        }

    def connect_to(self, name):
        """ Successive connection of Primitive to another """
        self.connection["to"] = name

    def connect_from(self, name):
        """ Pervious connection to this Primitive """
        self.connection["from"] = name

    def execute(self):
        """ Execute primitive """
        raise NotImplementedError("Please add your own logic!")

    def __repr__(self):
        """ Returns offical representation of the Primitive """
        text = "Name: {}\nCost: {}\nDuration: {}".format(
            self.name, self.cost, self.duration)
        return text
