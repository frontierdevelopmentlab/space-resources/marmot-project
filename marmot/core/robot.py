"""
`robot.py` contains numerious classes pertaining to representing basic
properties, advanced functions/methods, and multi-agent interactions.

The paper from this information is extracted from
https://www.ri.cmu.edu/pub_files/2015/6/Energy-Utilization-and-Energetic.pdf

"""
import json
import copy


class BasicRobot():
    """
    A Robot is an agent that has the following properties:

    Parameters
    ----------
    name : str
        pass
    battery : float
        pass
    status : bool
        pass
    weight : int
        pass
    power : int
        pass
    robot_type_id : int
        pass

    Attributes
    ----------
    name : str
        A string representing the customer's name.
    internal_batt :
        pass
    internal_temperature :
        pass
    status : boolean
        ON (True) and OFF(False)

    """
    C_ROVER = 2
    C_RELAY = 1
    C_LANDER = 0

    def __init__(self, name, robot_type_id=0, path_dict=True):
        """Return a robt object whose name is *name* with these variables"""
        self.property = {
            "name": name,
            "type": "lander",
            "battery": 100.0,
            "power": 10,
            "status": True,
            "mass": 10,  # kg
            "distance_traversable": 216,
            "epe": 30,
            "velocity": 0.01,  # m/s
            "drive_duty_cycle": 100,
            "energy_driving": 30,
            "init_x": 0,
            "init_y": 0,
            "comm_radius": 20,
            "c_rr": 0.15,
            "gravity": 9.81,
            "blender_x": 0.0,
            "blender_y": 0.0,
        }

        # Set robot type (0 = lander, 1 = relay, 2 = rover)
        self.robot_type = robot_type_id

        self.data = None  # Stores data information.

        # Dict version stores data for each path step.
        # NOTE: "s" key stores a tuple values for the paht point.
        if path_dict:
            self.path = {0: None}

        # List solely stores the path points in the list.
        # NOTE: Should be a list of tuples.
        else:
            self.path = []

        self.current_route = list()

    @property
    def robot_name(self):
        """ Robot name property """
        return self.property["name"]

    @robot_name.setter
    def robot_name(self, val):
        """ Robot name property setter """
        self.property["name"] = str(val)

    @property
    def robot_type(self):
        """ Robot type property """
        return self.property["type"]

    @robot_type.setter
    def robot_type(self, val):
        """ Robot type property setter """
        if val == BasicRobot.C_LANDER:
            self.property["type"] = "lander"

        elif val == BasicRobot.C_RELAY:
            self.property["type"] = "relay"

        elif val == BasicRobot.C_ROVER:
            self.property["type"] = "rover"

        else:
            raise ValueError("Not a valide robot type.")

    @property
    def battery(self):
        """ Battery property """
        return self.property["battery"]

    @battery.setter
    def battery(self, val):
        """ Battery property setter """
        self.property["battery"] = val

    @property
    def power(self):
        """ Power property """
        return self.property["power"]

    @power.setter
    def power(self, val):
        """ Power property setter """
        self.property["power"] = val

    @property
    def status(self):
        """ Robot status property """
        return self.property["status"]

    @status.setter
    def status(self, val):
        """ Robot status property setter """
        self.property["status"] = val

    @property
    def mass(self):
        """ Mass property """
        return self.property["mass"]

    @mass.setter
    def mass(self, val):
        """ Mass property setter """
        self.property["mass"] = val

    @property
    def distance_traversable(self):
        """ Distance traversable property """
        return self.property["distance_traversable"]

    @distance_traversable.setter
    def distance_traversable(self, val):
        """ Distance traversable property setter """
        self.property["distance_traversable"] = val

    @property
    def epe(self):
        """ Effective propulsion energy property (%) """
        return self.property["epe"]

    @epe.setter
    def epe(self, val):
        """ Effective propulsion energy property setter (%) """
        self.property["epe"] = val

    @property
    def velocity(self):
        """ Velocity property (m/s) """
        return self.property["velocity"]

    @velocity.setter
    def velocity(self, val):
        """ Velocity property setter (m/s) """
        self.property["velocity"] = val

    @property
    def drive_duty_cycle(self):
        """ Drive duty cycle property """
        return self.property["drive_duty_cycle"]

    @drive_duty_cycle.setter
    def drive_duty_cycle(self, val):
        """ Drive duty cycle property setter """
        self.property["drive_duty_cycle"] = val

    @property
    def energy_driving(self):
        """ Energy driving property """
        return self.property["energy_driving"]

    @energy_driving.setter
    def energy_driving(self, val):
        """ Energy driving property setter """
        self.property["energy_driving"] = val

    @property
    def init_x(self):
        """ Initial x position property """
        return self.property["init_x"]

    @init_x.setter
    def init_x(self, val):
        """ Initial x position property setter """
        self.property["init_x"] = val

    @property
    def init_y(self):
        """ Initial y position property """
        return self.property["init_y"]

    @init_y.setter
    def init_y(self, val):
        """ Initial y position property setter """
        self.property["init_y"] = val

    @property
    def comm_radius(self):
        """ Communication radius property """
        return self.property["comm_radius"]

    @comm_radius.setter
    def comm_radius(self, val):
        """ Communication radius property setter """
        self.property["comm_radius"] = val

    @property
    def c_rr(self):
        """ Terrain resistance coefficent property """
        return self.property["c_rr"]

    @c_rr.setter
    def c_rr(self, val):
        """ Terrain resistance coefficent property setter """
        self.property["c_rr"] = val

    @property
    def gravity(self):
        """ Gravity property (m/s^2) """
        return self.property["gravity"]

    @gravity.setter
    def gravity(self, val):
        """ Gravity property setter (m/s^2) """
        self.property["gravity"] = val

    @property
    def blender_x(self):
        """ Initial x position property """
        return self.property["blender_x"]

    @blender_x.setter
    def blender_x(self, val):
        """ Initial x position property setter """
        self.property["blender_x"] = val

    @property
    def blender_y(self):
        """ Initial y position property """
        return self.property["blender_y"]

    @blender_y.setter
    def blender_y(self, val):
        """ Initial y position property setter """
        self.property["blender_y"] = val

    def import_json(self, file_name):
        """ Import json file with data and/or properties. """
        assert isinstance(file_name, str), "popo"
        data = json.load(open(file_name))
        # Copy properties form JSON file into robot properties attribute.
        for prop in self.property:
            if prop in data:
                self.property[prop] = data[prop]

        if "path" in data:
            # if path is a list, regenerage tuples then append to list.
            self.path = []
            if isinstance(data["path"], list):
                for i in range(0, len(data["path"])):
                    self.path.append(tuple(data["path"][i]))

            # if path is a dict, set variable and regenerage tuples.
            elif isinstance(data["path"], dict):
                self.path = data["path"]
                for key in self.path:
                    self.path[key]["s"] = tuple(self.path[key]["s"])

    def export_json(self, file_name, include_path=False):
        """ Export json file with the robot's data and/or properties. """
        if include_path:
            data = copy.copy(self.property)

            # Convert list of tuples to a list of list [[x, y], ...]
            if isinstance(self.path, list):
                temp_path = []
                for path_point in self.path:
                    # Convert tuple to list for json file.
                    temp_path.append(list(path_point))

                data["path"] = temp_path

            # Convert the tuple within ["s"] dict key from tuple to list
            elif isinstance(self.path, dict):
                data["path"] = copy.copy(self.path)
                for key in data["path"]:
                    # Convert tuple to list for json file.
                    data["path"][key]["s"] = list(data["path"][key]["s"])

            if file_name[-5:] != ".json":
                json.dump(data, open(file_name + ".json", "w"))

            else:
                json.dump(data, open(file_name, "w"))

        else:
            if file_name[-5:] != ".json":
                json.dump(self.property, open(file_name + ".json", "w"))

            else:
                json.dump(self.property, open(file_name, "w"))


class AdvancedRobot(BasicRobot):
    """
    Advanced Robot Class

    """
    def __init__(self, name, robot_type_id=0):
        super().__init__(self, name, robot_type_id)

    def increase_battery(self, value):
        """ Return the robot battery status. """
        self.battery += value
        return self.battery

    def decrease_battery(self, value, shadow_effect_weight=1.0):
        """
        Return the battery status after decreasing due to shadow effects. By
        default, the effects of the shadow are not weighted extra.

        Properties
        ----------
        value : float
            Battery charge decrease value.
        shadow_effect_weight : float, optional
            The effect of shadows that can effect a lunar robot. By default
            this is kept at 1.0, meaning there is no effect.

        """
        self.property["battery"] -= shadow_effect_weight * value

    def energy_consumption(self):
        """ Something """
        self.energy_driving = ((self.c_rr * self.mass * self.gravity *
                                self.distance_traversable) / self.epe) + \
                                (0.1 * (self.distance_traversable / \
                                        (self.velocity *
                                         self.drive_duty_cycle)))
        return self.energy_driving

    def achievable_range(self, power):
        """ Something """
        upper = self.velocity * self.energy_driving * self.drive_duty_cycle * \
             self.epe
        down = (self.velocity * self.c_rr * self.mass * self.gravity *
                self.drive_duty_cycle) + (100 * self.epe)

        rover_range = upper / down
        return rover_range

    def get_reachable_nodes(self, route):
        """
        I associated each node edge to 1 step, it needs to be updated to the
        relation pixel/meters
        """
        #Terrain resistance coeficient
        self.drive_duty_cycle = 100
        self.velocity = 0.1
        total_distance = int(self.achievable_range(1))
        #print("The total jumps needed are ",
        #      len(route),
        #      "The maximum nodes traversable with this battery are ",
        #      total_distance))

        return total_distance

    def set_initial_position(self, x_val, y_val):
        """ Set initial position of robot. """
        self.property["init_x"] = x_val
        self.property["init_y"] = y_val

    def get_initial_position(self):
        """
        I associated each node edge to 1 step, it needs to be updated to the
        relation pixel/meters
        """
        return (self.property["init_x"], self.property["init_y"])


    def set_robot_current_route(self, val):
        """ Set current route of robot. """
        self.current_route = val

    def get_robot_current_route(self):
        """ Get current route of robot. """
        return self.current_route


class RobotTeam():
    """
    Class for a robot team.

    """
    C_ROVER = 2
    C_RELAY = 1
    C_LANDER = 0

    def __init__(self, num_of_robots, robot_names_list, robot_type_list,
                 advanced=False):
        """
        Something
        """
        self.num_of_robots = num_of_robots
        self.__robot_types = [self.C_LANDER, self.C_RELAY, self.C_ROVER]

        # Check input arguments
        assert isinstance(num_of_robots, int)
        assert isinstance(robot_names_list, list)
        assert isinstance(robot_type_list, list)
        self._check_robot_types(robot_type_list)

        # Create robot instances
        self.robot = {}
        for name, robot_type, i in zip(robot_names_list,
                                       robot_type_list,
                                       range(1, num_of_robots + 1)):
            if advanced:
                self.robot[i] = AdvancedRobot(name, robot_type)

            else:
                self.robot[i] = BasicRobot(name, robot_type)

    def _check_robot_types(self, robot_type_list):
        """ Checks robot types in list """
        for i in robot_type_list:
            if i not in self.__robot_types:
                raise ValueError(str(i) +  " is not a valid robot type!")
