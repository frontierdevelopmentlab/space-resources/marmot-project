
"""
Created on Wed Aug  1 13:03:45 2018

path planning


main function:
    initializes the graph
    # optional: run a mission_relay instance

see function:
    mission_relay
for the latest mission example

Author: Drew Bishel

"""

import time
import random
import itertools

import networkx as nx
from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
import mapstery
from graphery import graph_generator

import marmot.tools.order_waypoints as way
import marmot.tools.comm_bubble as comm


def a_star(graph, pos_start, pos_end, dist_func, weight_name="weight"):
    """ NetworkX A* Search Algorithm. """
    return nx.astar_path(graph, pos_start, pos_end, dist_func,
                         weight=weight_name)


def a_star_with_replan(start, end, v_avg, d_t, graphs, dist_func):
    """
    NetworkX A* Search with Replane Capabilities.

    Just like a normal, built-in network A* search, but a replan is done after
    `d_steps`. This number of replans is calculated from `d_t` and `v_avg`.

    Parameters
    ----------
    start : tuple
        Path start point in the form of a coordinate tuple.
    end : tuple
        Path end point in the form of a coordinate tuple.
    v_avg: int/float
        Average velocity of the rover in units of pixels/timestep.
    d_t : int
        Replan route for every T timestep.
    graphs : list of nx.classes.graph.Graph()
        List of shadow-integrated maps. Each map must integrate >= T timesteps.
    dist_func : func
        Takes two (x, y) tuples and returns distance between.

    Returns
    -------
    kept_path : list of tuples
        A path that is replanned every T timesteps to overcome time-dependent
        shadows.

    """
    # Larger T (replan time horizon / map integration T)
    # a_star(graphy, pos_start, pos_end, dist_func, weight_name="weight")
    # [(),(),(),(),...,()]

    # take care to make sure dist units are in pixels (v = pixels / timestep,
    # T=t timesteps)
    # Will need to make the next replan at this distance after T timesteps
    d_max = int(v_avg * d_t)
    kept_path = []

    last_point = start
    max_iters = 2500
    iters = 0
    time_step = 0
    print("d_max: " + str(d_max))
    while iters < max_iters and time_step < len(graphs):
        print("Working on iter: " + str(iters))
        print("Time Step: " + str(time_step))
        if iters == 0:
            remaining_path = a_star(graphs[time_step], start, end, dist_func,
                                    weight_name="weight")

        else:
            remaining_path = a_star(graphs[time_step], last_point, end,
                                    dist_func, weight_name="weight")

        if len(remaining_path) < d_max and iters is not 0:
            kept_path += remaining_path[1:]  # don't double add the start point
            break

        elif len(remaining_path) < d_max and iters is 0:
            kept_path += remaining_path[:]  # keep the start point
            break

        # d_max should be an index now
        else:
            kept_path += remaining_path[1:d_max]
            time_step += d_t
            iters += 1
            last_point = kept_path[-1]

    return kept_path


def dist_euc(node_1, node_2):
    """ Simple Euclidean Distance Function """
    return np.sqrt(np.power(node_2[0] - node_1[0], 2) +
                   np.power(node_2[1] - node_1[1], 2))


def path_time(path, avg_vel, dist_per_pixel=1):
    """
    Calculates the time taken to travel a path.

    Parameters
    ----------
    path : list of tuples
        pass
    avg_vel : int/float
        pass

    Returns
    -------
    time_taken : int/float
        Total time to travel the input path.

    """
    dist = len(path) * dist_per_pixel # default is 1 if velocity is in units of pixels/timestep
    # avg_vel = pixels/sec --- time will depend on avg_vel and dist_per_pixel units
    time_taken = dist / avg_vel     # e.g. (t = (10 pixels) / (1 pixel/timestep)) = 10 timesteps
    return time_taken


def find_closest_point(pos, points, dist_func):
    """
    Find the closest point (out of many `points`) to `pos`.

    Parameters
    ----------
    pos : tuple
        The initial position as an (x, y) coordinate.
    points : list of tuples
        pass
    dist_func : func
        A Euclidean distance that takes coordinate tuples.

    Returns
    -------
    latest_pos : tuple
        The closest position from the list to the desired position, `pos`.

    """
    dist_keep = dist_func(pos, points[0]) #initialize to some distance
    latest_pos = points[0]
    for i, _ in enumerate(points):
        dist = dist_func(pos, points[i])
        if dist < dist_keep:
            dist_keep = dist
            latest_pos = points[i]

    return latest_pos


def relay_ontime(start, waypoints, ends, max_time, graphy, dist_func, max_samples=15):
    """
    Multi-robot path planning for multiple waypoints on a graph.

    Bigger Explanation

    Parameters
    ----------
    start : tuple
        Start point of robotic agent as an (x, y) coordinate.
    waypoints : list of lists
        Each list within the `waypoints` list is a category of waypoints, made
        up of (x, y) coordinate tuples, by rank of importance.
    ends : list
        A list of possible end point (x, y) coordinate tuples.
    max_time : int/float
        The maximum time allotted to get to an end point.
    graphy : nx.classes.graph.DiGraph()
        A digraph on which the search is applied.
    dist_func : func
        Simple euclidean distance function that uses (x, y) coordinate tuples.

    Returns
    -------
    working_paths : list
        A set of paths that try to visit points-of-interest and get to an
        end point without exceeding the max time requirement.

    """
    # relay rover params
    avg_vel = 1

    # map params
    dist_per_pixel = 1

    end = find_closest_point(start, ends, dist_func)
    first_try = a_star(graphy, start, end, dist_func)
    print("closest point (in 'ends' variable): " + str(end))
    t_val = path_time(first_try, avg_vel, dist_per_pixel=dist_per_pixel)
    working_paths = []

    if t_val > max_time:
        print("Relay rover will not arrive by given max_time")
        return []

    else:  # if first was good, see if you can visit extra sites before max_time
        working_paths.append(first_try)
        count = 1  # says that a* path found at least 1 working path so far
        for j in range(len(waypoints)-1):
            # Choose a small set of waypoints to find a path through; insert
            # start point as beginning point
            wps = waypoints[:j+1]
            wps = list(wps)
            wps.insert(0, [start])

            # Sample from 100 possible endpoints (or the whole list of
            # endpoints if < max_samples)
            n_samples = min(max_samples, len(ends))
            endpoint_samples = random.sample(ends, n_samples)
            for i in range(n_samples):
                sample_path = []
                sample_waypoints = way.min_waypoints_endpoint(
                    endpoint_samples[i], wps, dist_func)
                # make an a* path between waypoints
                for k in range(0, len(sample_waypoints) - 1):
                    sample_path += a_star(graphy, sample_waypoints[k],
                                          sample_waypoints[k+1], dist_func)

                # Calculate the time taken along the path
                t_val = path_time(sample_path, avg_vel,
                                  dist_per_pixel=dist_per_pixel)
                if t_val < max_time:
                    working_paths.append(sample_path)
                    count += 1

        # if no new paths were found end loop early.
        if len(working_paths) == count:
            return working_paths

    return working_paths


def plot_waypoints(waypoints, impath=None, color='b', mark=".", size=.2,
                   facecolors=None):
    """ Plot all the waypoints on the image. """
    # Each waypoint is (x,y)
    x_points = [waypoint[0] for waypoint in waypoints]
    y_points = [waypoint[1] for waypoint in waypoints]

    if impath is not None:
        img = Image.open(impath)
        plt.imshow(img, cmap='gray')

    if facecolors is not None:
        plt.scatter(x_points, y_points, marker=mark, c=color, s=size,
                    facecolors=facecolors)

    else:
        plt.scatter(x_points, y_points, marker=mark, c=color, s=size)


def init_graphs_shadowmaps():
    """ Initialize Graph with Shadowmaps """
    graphs = []
    cost_map = mapstery.Map("images/slope-1-shadow-2-169-h24.gtif")
    n_graphs = 3  # 169 -> back to 2

    my_graph = graph_generator.GraphGenerator()

    arr = cost_map.get_band(2).ReadAsArray()
    [rows, cols] = np.shape(arr)  # all maps should have the same shape
    print("Initializing 1st graph ... " + "\n" + "... "*5)

    t_0 = time.time()
    g_1 = my_graph.graph_grid_init(arr)
    t_1 = time.time()
    print("... finished. Took " + str(t_1 - t_0) + " seconds.")
    print("Loading map at subsequent timesteps...")

    # Append graph to list of other graphs.
    graphs.append(g_1)

    for i in range(3, n_graphs + 1):
        if (i % 10) == 0:
            print("Loaded " + str(i) + " maps.\nLast map:\n   " + \
                  str(t_3 - t_2) +  " seconds for M.get_band(i).ReadAsArray()")
            print("   " + str(t_4 - t_3) +  " seconds for " + \
                  "my_graph.load_map_grid(graphs[-1], rows, cols,arr)")

        t_2 = time.time()
        arr = cost_map.get_band(i).ReadAsArray()
        t_3 = time.time()
        graphs.append(my_graph.load_map_grid(graphs[-1], rows, cols, arr))
        t_4 = time.time()

    print("$$$ Maps finished loading $$$\n" + "_"*10)

    return my_graph, graphs, arr, rows, cols  #, fname


def init_map_slope_shadow():
    """ Initialize Slope-Shadow Map """
    t_i = time.time()
    # get slope and shadow maps
    graphs = []
    arrs = []
    cost_map = mapstery.Map("images/slope-1-shadow-2-169-h24.gtif")
    # init graph
    my_graph = graph_generator.GraphGenerator()
    arr = cost_map.get_band(2).ReadAsArray()
    [rows, cols] = np.shape(arr) # all maps should have the same shape

    # combine slope + shadow for each map
    slopemap = cost_map.get_band(1).ReadAsArray()
    power = 1.03
    slopemap = slopemap#**power # make larger slops more dangerous

    # graphs.append(slopemap)
    n_graphs = 5
    band_range = range(2, n_graphs+1)
    for i in band_range:
        shadow_i = cost_map.get_band(i).ReadAsArray()
        shadow_i = np.max(shadow_i) - shadow_i  # invert
        shadow_i = shadow_i  #**(power+0.03)
        new_band_arr = slopemap + shadow_i
        if i == band_range[0]:
            graph = my_graph.graph_grid_init(new_band_arr)
            arrs.append(arr)

        else:
            graph = my_graph.load_map_grid(graphs[-1], rows, cols, new_band_arr)
            arrs.append(new_band_arr)
            #graphs.append(shadow_i)

        #band_name = "SlSh_" + str(i)
        #M.add_band(new_band_arr, band_name, new_band=True, band_info="shadow + slope")
        graphs.append(graph)
        print("\nFinished map # " + str(i-1))

    t_f = time.time()
    fin_time = (t_f - t_i)/60.
    print("Init took " + str(fin_time) + " minutes")
    return graphs, arrs, my_graph, cost_map, [rows, cols]


def mission_relay(graphy, rows, cols, arrs):
    """ Something """
    sci_start = (780, 762)
    sci_end = (951, 814)

    rel_start = (839, 862)

    sci_rover_path = a_star(graphy, sci_start, sci_end, dist_euc,
                            weight_name="weight")
    avg_vel = 1
    t_sci_path = path_time(sci_rover_path, avg_vel, dist_per_pixel=1)

    r_1 = 7
    r_2 = 5
    r_3 = 4

    # x = 950-1200, y = 600-800
    rel1 = [(random.randint(850, 930), random.randint(834, 880)) for k in range(r_1)]
    rel2 = [(random.randint(869, 888), random.randint(740, 780)) for k in range(r_2)]
    rel3 = [(random.randint(789, 812), random.randint(834, 880)) for k in range(r_3)]

    # =========================================================================
    ## x = 950-1200x, y = 330-470
    # rel1 = [(random.randint(1010, 1100),random.randint(330, 470)) for k in range(r1)]
    # rel2 = [(random.randint(1050, 1150),random.randint(330, 470)) for k in range(r2)]
    # rel3 = [(random.randint(1100, 1200),random.randint(330, 470)) for k in range(r3)]
    # =========================================================================

    # =========================================================================
    ## POI Waypoints
    # rel1 = [(990, 440), (992, 442), (999, 444), (1008, 432)]
    # rel2 = [(940, 420), (1000, 442), (1300, 420), (1120, 429)]
    # rel3 = [(1208, 340), (1190, 320), (999, 318)]
    # =========================================================================
    rel_waypoints = [rel1, rel2, rel3]

    # what's the antenna height and communication radius of the science rover?
    antenna_height = 0.000
    rad = .03
    min_signal = .08
    # convert discrete row-col coords to float coords for calling blender via comm
    sci_end_w, sci_end_h = sci_end
    sci_x = 10.58 * ((2. / cols) * sci_end_w - 1.)
    sci_y = 7.79 * ((-1. * 2. / rows) * sci_end_h + 1.)

    # write visibility image; threshold to get communication envelope; find outer-most points

    out_img_path = "/Users/arcturus/Desktop/MAS/FDL_go/" + \
        "simulation_environment/Application/images/visib.png"
    source_blender_path = "/Users/arcturus/Desktop/MAS/FDL_go/" + \
        "moon-hermite-v3-2.blend"
    blender_pyscript_path = "/Users/arcturus/Desktop/MAS/FDL_go/" + \
        "simulation_environment/data_makers/visibility.py"
    blender_app_path = "/Applications/Blender/blender.app"

    print("Calling blender")
    im2path = comm.write_visibility(
        sci_x, sci_y, antenna_height, rad, out_img_path=out_img_path,
        input_blender_file_path=source_blender_path,
        blender_pyscript_path=blender_pyscript_path,
        blender_app_path=blender_app_path)

    print("Editing saved blender file")
    comm_map = comm.comm_threshold(im2path, min_signal)

    # Available indices above threshold but far from sci-rover
    inds = comm.indices_available(comm_map)
    inds_map = comm.avail_indices_map(inds, np.shape(comm_map))

    possible_endpoints = inds
    max_time = t_sci_path

    working_paths = []
    timetaken = 0
    count = 0
    perms = list(itertools.permutations(rel_waypoints))
    avg_len_waypointtype = sum([len(x) for x in rel_waypoints]) / len(rel_waypoints)
    eta = avg_len_waypointtype*len(perms)*1.8
    print("Very rough ETA till completion: " + str(eta) + " minutes")

    for perm in perms:
        t_1 = time.time()
        working_paths += relay_ontime(rel_start, perm, possible_endpoints,
                                      max_time, graphy, dist_euc,
                                      max_samples=30)
        t_2 = time.time()
        tim = (t_2 - t_1) / 60.
        timetaken += tim
        count += 1
        print("set # " + str(count) + " of waypoints finished testing..." + \
              "\nTime taken: " + str(tim) + " mins")
        print("Total time taken so far: " + str(timetaken) + " mins\n")
        est = abs((len(perms)-count)*tim)
        trunc_string = str(est)[0:4]
        print("Rough eta: " + trunc_string + " minutes\n\n")

    print("There are " + str(len(working_paths)) +
          " on-time paths found for the relay rover. ")
    print("-- search time -- " + str(timetaken)[0:4])
    ############################################################
    ##### only plotting below here.
    ############################################################

    impath = 'images/hermite_a_dem_mosaic_1m_v3-255--1336x984.png'
    # image and science rover path plotted first
    plt.imshow(arrs, cmap='gray')
    plot_waypoints(sci_rover_path, color='b')

    from matplotlib import colors as mcolors
    colors = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)
    markers = ["o", ".", ",", "v", "8", "s", "p", "*", "x", "d", "H", "+"]
    redish_colors = ['r', 'm', colors["crimson"], colors["darkred"],
                     colors["firebrick"], colors["maroon"],
                     colors["orangered"], colors["palevioletred"]]
    greenish_colors = [colors["yellowgreen"], colors["darkgreen"],
                       colors["limegreen"], colors["turquoise"],
                       colors["teal"], colors["palegreen"], colors["green"]]
    yellow_green_azure = [colors["yellow"], colors["green"], colors["azure"]]


    # plot the reduced communication envelope
    plot_waypoints(possible_endpoints, color=colors["darkcyan"])

    # plot the potential relay rover paths
    n_val = 1
    for path in working_paths:
        if n_val > len(redish_colors):
            break # give up when you run out of colors to plot

        plot_waypoints(path, color=redish_colors[n_val-1],
                       mark=markers[n_val%len(markers)])
        n_val += 1

    # plot the potential POI relay rover waypoints
    n_val = 1
    for waypoint in rel_waypoints:
        colrs = yellow_green_azure + greenish_colors + redish_colors
        if (n_val % len(colrs)) == 0:
            n_val = 1

        plot_waypoints(waypoint, color=colrs[n_val-1], mark=markers[0], size=7)
        n_val += 1

    # plot sci rover start&end, rel rover start
    plot_waypoints([sci_start, sci_end], color='gold', mark='*', size=7)
    plot_waypoints([rel_start], color='goldenrod', mark='d', size=7)

    # output a dictionary of stuff we might want at the end
    communication_info = {"comm map": comm_map,
                          "comm map subsection": inds,
                          "antenna height": antenna_height,
                          "comm radius": rad,
                          "signal threshold": min_signal}
    out = {"science path": sci_rover_path,
           "relay paths": working_paths,
           "relay waypoints": rel_waypoints,
           "comm": communication_info}

    return out


def visited_wps_greatEqsTo(n_thresh, paths, waypoint_sets):
    """ returns all the paths that visit n_thresh or more waypoints in the nested waypoints list
    :param n_thresh: min # waypoints to return
    :param paths: list of tuples; paths to look through
    :param wsets: list of lists of tuples; sets of waypoints by waypoint type
    """
    longerpaths = []
    matched_wps = []
    for path in paths:
        pwps = []
        for waypoint_set in waypoint_sets:
            for waypoint in waypoint_set:
                if waypoint in path:
                    pwps.append(waypoint)

        if len(pwps) >= n_thresh:
            longerpaths.append(path)
            matched_wps.append(pwps)

    return longerpaths, matched_wps


def main():
    """ Testing the above functions. """
    # in case your python console has already run init_graph()
    # copy paste the following function and its args into the console to save ~ 1min
    # mission_relay(my_graph, graphy, arr, rows, cols, fname)
    initg = False
    if initg:
        #my_graph, graphy, arr, rows, cols, fname = init_graph()
        #mission_relay0(my_graph, graphy, arr, rows, cols, fname)

        dist_func = dist_euc
        ##graphs, arrs, my_graph, M, rowscols = init_map_slope_shadow()
        my_graph, graphs, arr, rows, cols = init_graphs_shadowmaps()
        out2 = mission_relay(graphs[1], rows, cols, arr)

    #science_path = out1["science path"]
    #relay_paths = out1["relay paths"]
    #relay_waypoints = out1["relay waypoints"]
    #comm_stuff = out1["comm"]
                #= {"comm map":comm_map,"comm map subsection":inds,
                #"antenna height":antenna_height, "comm radius": rad,
                #"signal threshold":min_signal}


if __name__ == "__main__":
    main()
