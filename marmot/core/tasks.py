"""
Tasks for an Agent

Author: Zahi Kakish (GitHub/GitLab: zmk5)

"""
from marmot.core.primitives import Primitive


class Task():
    """
    A complete task consisting of primatives.
    """
    def __init__(self, name):
        self.name = name
        self.composition = {}
        self._param = {
            "count": 0,
            "total_primitives": 0,
            "keys": [],
        }
        self._total_cost = 0
        self._total_duration = 0

    @property
    def cost(self):
        """ Cost of executing Task (all primitives) """
        self._update_total_cost()

        return self._total_cost

    @property
    def duration(self):
        """ Time duration for executing Task (all primitives) """
        self._update_total_duration()

        return self._total_duration

    def add_primitive(self, name, cost, duration):
        """ Add a primative to constitute task """
        self.composition[name] = Primitive(
            name, cost, duration)
        self._param["total_primitives"] += 1
        self._param["keys"].append(name)

        # Update total cost and duration of task
        self._update_total_cost()
        self._update_total_duration()

    def remove_primitive(self, name):
        """ Remove a primitive from a task """
        try:
            self.composition.pop(name)
            self._param["keys"].remove(name)
            self._param["total_primitives"] -= 1

            # Update total cost and duration of task
            self._update_total_cost()
            self._update_total_duration()

        except KeyError:
            raise KeyError("Primitive `{}` not part of task".format(name))

        except ValueError:
            raise ValueError("Primitive `{}` not part of task".format(name))

    def execute(self):
        """ Execute task """
        raise NotImplementedError("Please add your own logic!")

    def _update_total_cost(self):
        """ Update the total cost of the task """
        self._total_cost = 0
        for primitive in self._param["keys"]:
            self._total_cost += self.composition[primitive].cost

    def _update_total_duration(self):
        """ Update the total duration of the task """
        self._total_duration = 0
        for primitive in self._param["keys"]:
            self._total_duration += self.composition[primitive].duration

    def __iter__(self):
        """ Iterator for primitives that consititute a task """
        self._param["count"] = 0
        return self

    def __next__(self):
        """ Iterate through the primitives of a task """
        if self._param["count"] < self._param["total_primitives"]:
            next_key = self._param["keys"][self._param["count"]]
            self._param["count"] += 1

        else:
            raise StopIteration

        # Return next primitive
        return self.composition[next_key]

    def __repr__(self):
        """ Returns offical representation of the Task """
        text_1 = "Task: {}\nContains: {}\nPrimitives: {}".format(
            self.name, self._param["total_primitives"], self._param["keys"])
        text_2 = "\nCost: {}\nDuration: {}".format(
            self._total_cost, self._total_duration)
        return text_1 + text_2
