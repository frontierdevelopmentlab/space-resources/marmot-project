"""
Plot Graphs and paths for multiple robots.

Example
--------
>>> import networkx as nx
>>> graph = nx.grid_graph(dim=[10, 10])
>>> path = [(0, 0), (0, 1), (1, 1), (2, 1)]
>>> generate_point_locations(graph)
>>> plot_path(path)
>>> show_paths(0, 10, 0, 10)

Author: Zahi Kakish (zmk5)

"""
import matplotlib.pyplot as plt
import networkx as nx


def plot_path(path, bias=0.5, color="r"):
    """ Plot the path of a robot. """
    for i in range(1, len(path)):
        try:
            plt.plot([path[i-1]["s"][0] + bias, path[i]["s"][0] + bias],
                     [path[i-1]["s"][1] + bias, path[i]["s"][1] + bias],
                     color + "-")

        except TypeError:
            plt.plot([path[i-1][0] + bias, path[i][0] + bias],
                     [path[i-1][1] + bias, path[i][1] + bias], color + "-")


def generate_point_locations(graph, bias=0.5, color="b"):
    """ Generate node point locations for plot """
    assert isinstance(graph, nx.classes.graph.Graph), \
        "Input argument is not a networkx graph."

    # Create x and y point set for matplotlib to plot.
    x_points = []
    y_points = []
    for point in graph.nodes():
        x_points.append(point[0] + bias)
        y_points.append(point[1] + bias)

    plt.plot(x_points, y_points, color + "o")


def show_paths(x_axis_min=0, x_axis_max=100, y_axis_min=0, y_axis_max=100):
    """ Finish plotting information and show paths. """
    plt.grid()
    plt.axis([x_axis_min, x_axis_max, y_axis_min, y_axis_max])
    plt.xticks([])
    plt.yticks([])
    plt.show()
