"""
Useful map utilities
"""
from PIL import Image
import scipy.misc as smp

def save_image_path(file_name, path_nodes, map_size, collision,
                    output_file_name="output_map_file.png"):
    """ Something """
    obstacle_map_size_1 = map_size - 1
    data_image = Image.open(file_name).convert("RGB")
    p_x = data_image.load()

    for node in path_nodes:
        print(node)

        p_x[(int(node[0]/2)),
            (obstacle_map_size_1 - int(node[1]/2))] = (0, 0, 254)

    if collision is True:
        print("There is a route conflict")

    data_image.save(output_file_name)
    img = smp.toimage(data_image)  # Create a PIL image
    img.show()
