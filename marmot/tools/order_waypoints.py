"""
Created on Tue Jul 24 11:25:04 2018

Choose the waypoint order

waypoint = coordinate = (x,y)

input:
    green  = [(w1x, w1y), (w2x, w2y), (w3x, w3y)]
    yellow = [(w1x, w1y), (w2x, w2y), (w3x, w3y)]
    gray   = [(w1x, w1y), (w2x, w2y), (w3x, w3y)]

output:
    gives 3 way points in a heuristic/approx. min path ordering; one from green,
    one from yellow, one from gray

    out = [yellow[2], green[4], gray[0]]

@author: Drew

"""
import itertools


GREEN = [(4, 7), (40, 70)]
YELLOW = [(15, 24), (14, 20), (40, 69)]
GRAY = [(8, 10), (4, 4), (42, 69)]

ROVER_POS = [(20, 20)]
WAYPOINTS = [ROVER_POS, GREEN, YELLOW, GRAY]

"""
input: [(roverXY), (way1XY), (w2X,w2Y),...]
output: the shortest path that goes through 1 green, 1 yellow, 1 gray
"""

def min_waypoints(waypoints, dist_func):
    """
    Finds minimum distance between waypoints in each category.

    Takes a list of lists, each inner list is a set of points of the same
    category the algorithm chooses one waypoint from each category and finds the
    shortest distance that covers one point from each category. If a point is
    "must-visit", then its own category list should only have that point in it.
    [[(roverX, Y)], [(way1X,1Y), (w2X,w2Y)], [(mustvisitX,Y)],...]

    Parameters
    ----------
    waypoints : list of lists of tuples
        First tuple in the first list (with only 1 tuple) is the initial rover
        position.
    dist_func : func
        A function that takes two tuples/positions and calculates the distance
        between them.

    """
    w_p = waypoints[1:]  # without initial rover position

    # possible permutations of paths
    possible_permutations = [x for x in itertools.permutations(w_p)]
    candidates = []
    candidate_dists = []

    for i, _ in enumerate(possible_permutations):
        # an element here is a (permuted) order of colors to search through
        possible_cp = list(possible_permutations[i]).copy()

        # put the start point back up front
        possible_cp.insert(0, waypoints[0])

        # given a specific order of colors, return shortest path
        candidate = min_ordered_wps(possible_cp, dist_func)

        # add shortest path, try new order of colors
        candidate_dists.append(path_dists(candidate, dist_func))
        candidates.append(candidate)

    return candidates[candidate_dists.index(min(candidate_dists))]


def path_dists(path, dist_func):
    """ Returns the sum of distances between multiple waypoints. """
    total_sum = 0
    for j in range(0, len(path) - 1):
        total_sum += dist_func(path[j], path[j+1])

    return total_sum


def min_ordered_wps(waypoints, dist_func):
    """
    function chooses one waypoint to visit in each list, in the order given
    chooses the one that minimizes dist_func distance.

    Parameters
    ----------
    waypoints : list of lists of tuples
        [[(x,y)], [(x2,y2),(X2,Y2),(2x,2y)], [(x3,y3),(X3,Y3)], ...]
    dist_func : func
        A function that takes two tuples/positions and calculates the distance
        between them.

    """
    possibilities = [x for x in itertools.product(*waypoints)]
    dists = []

    for i, _ in enumerate(possibilities):
        # calculate naive weight-distances between each waypoint in each
        # possible combination
        dists.append(path_dists(possibilities[i], dist_func))

    # return the shortest waypoints that visits all 1 green, 1 yellow, 1 gray
    return possibilities[dists.index(min(dists))]


def min_waypoints_endpoint(endpoint, waypoints, dist_func):
    """
    Same as min_waypoints() except that the endpoint is the last point.

    Parameters
    ----------
    endpoint :
        tuple (x,y) of the point the rover needs to end at
    waypoints :
        categories of waypoints in between,
        [[rover init_position], [tup1, tup2], [tupl1,...]]
    dist_func : func
        A function that takes two tuples/positions and calculates the distance
        between them.

    """
    w_p = waypoints[1:]  # without initial rover position

    # possible permutations of paths
    possible_permutations = [x for x in itertools.permutations(w_p)]
    candidates = []
    candidate_dists = []

    for i, _ in enumerate(possible_permutations):
        # an element here is a (permuted) order of colors to search through
        possible_cp = list(possible_permutations[i]).copy()

        # put the start point back up front
        possible_cp.insert(0, waypoints[0])

        # given a specific order of colors, return shortest path
        candidate = min_ordered_wps_endpoint(endpoint, possible_cp, dist_func)

        # add shortest path, try new order of colors
        candidate_dists.append(path_dists(candidate, dist_func))
        candidates.append(candidate)

    return candidates[candidate_dists.index(min(candidate_dists))]


def min_ordered_wps_endpoint(endpoint, waypoints, dist_func):
    """
    Same as min_ordered_wps() except that the endpoint is the last point.

    Parameters
    ----------
    endpoint :
        the point that must be visited last
    waypoints :
        other waypoints in between
    dist_func : func
        A function that takes two tuples/positions and calculates the distance
        between them.

    """
    possibilities = [x for x in itertools.product(*waypoints)]
    dists = []

    for i, _ in enumerate(possibilities):
        # calculate naive weight-distances between each waypoint in each
        # possible combination.
        possible_path = possibilities[i]
        possible_path = list(possible_path)
        possible_path.append(endpoint)
        possibilities[i] = possible_path
        dists.append(path_dists(possible_path, dist_func))

    # return the shortest waypoints that visits all 1 green, 1 yellow, 1 gray
    return possibilities[dists.index(min(dists))]
