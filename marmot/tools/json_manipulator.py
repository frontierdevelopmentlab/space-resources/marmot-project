"""

"""
import json
import geojson


class JSONManipulator:
    """
    JSON Manipulator Class
    """
    def __init__(self, filename):
        print("Class constructor: json_mainupulator")
        self.v_jsonfile = filename
        _json_file = open(self.v_jsonfile)
        self.json_data = json.load(_json_file)

        self.DEBUG = False

    def get_zone_by_color(self, color):
        """
        This method is returning the origin (x,y) pixels, the height and the
        width of the `get_zone_by_color`.
        """
        mylist = list()
        # Use the new datastore datastructure
        for i in self.json_data[color]:
            #print ("Value x:", i["pixel"]["x"], " Value y: ", i["pixel"]["y"])
            value = (i["origin"]["x"], i["origin"]["y"],
                     i["origin"]["height"], i["origin"]["width"])
            mylist.append(value)

        return  mylist

    def get_list_of_origins_by_color(self, color):
        """
        This method is returning the list if origin (x,y) pixels

        Parameters
        ----------
        color : int
            Color for which to get list of origins.

        """
        mylist = list()
        # Use the new datastore datastructure
        for i in self.json_data[color]:
            value = (i["origin"]["x"], i["origin"]["y"])
            mylist.append(value)

        return  mylist

    def get_robot_list(self):
        """ Something """
        mylist_scientific = list()
        mylist_relay = list()
        mylist_lander = list()

        # Use the new datastore datastructure
        for robot in self.json_data["scientific"]:
            value = (robot["model"]["id"],
                     robot["model"]["origin"]["x"],
                     robot["model"]["origin"]["y"],
                     robot["model"]["blender_coord"]["x"],
                     robot["model"]["blender_coord"]["y"],
                     robot["model"]["mass"],
                     robot["model"]["type_name"])
            mylist_scientific.append(value)

        for robot in self.json_data["support"]:
            value = (robot["model"]["id"],
                     robot["model"]["origin"]["x"],
                     robot["model"]["origin"]["y"],
                     robot["model"]["blender_coord"]["x"],
                     robot["model"]["blender_coord"]["y"],
                     robot["model"]["mass"],
                     robot["model"]["type_name"])
            mylist_relay.append(value)

        for robot in self.json_data["lander"]:
            value = (robot["model"]["id"],
                     robot["model"]["origin"]["x"],
                     robot["model"]["origin"]["y"],
                     robot["model"]["blender_coord"]["x"],
                     robot["model"]["blender_coord"]["y"],
                     robot["model"]["mass"],
                     robot["model"]["type_name"])
            mylist_lander.append(value)

        return  mylist_scientific, mylist_relay, mylist_lander

    def get_robot_json(self):
        """ Something """
        return self.json_data

    @staticmethod
    def write_geojson(features, filename='output-points.geojson'):
        """ Something """
        # Feature is a shapely geometry type.
        geom_in_geojson = geojson.LineString(
            coordinates=features, properties={})

        with open(filename, 'w') as outfile:
            geojson.dump(geom_in_geojson, outfile)

        outfile.close()
