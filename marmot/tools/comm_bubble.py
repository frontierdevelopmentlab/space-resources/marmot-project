"""
Created on Mon Jul 30 12:35:45 2018

comm_threshold(imfile, thresh, out_path = None, save = True)
                imfile; path of the source image (taken from write_visibility)
                thresh; img pixels below threshold value are set to 0 (comm "works" otherwise)
                out_path; default is a modified version of imfile path -- can specify
                save; default = True, False will not save the communication bubble image

                returns image

calls blender API (which in turn uses visibility.py)

write_visibility(args)
### calls blender and renders/saves a point source image
                x,y position of communication source
                antenna_height
                radius

                out_imgpath --> blender output path .png file
                source_blendpath --> blender takes the DEM from this path
                blendPyScript  --> visibility.py  blender uses this script to 
                                   render the point source
                blendApp --> location of the blender application

                returns img_outpath

Author: Drew Bishel

"""

from sys import platform
from subprocess import call
from matplotlib.image import imread
from matplotlib import pyplot as plt
import numpy as np


def communications_step(x_val, y_val, radius=.1, antenna_height=.5,
                        imgpath="./images/visib.png"):
    """ Something """
    return write_visibility(x_val, y_val, antenna_height, radius,
                            out_img_path=imgpath)


def write_visibility(x_val, y_val, antenna_height, radius, out_img_path=None,
                     input_blender_file_path=None, blender_pyscript_path=None,
                     blender_app_path=None):
    """ Something """
    # these paths need fixing

# =============================================================================
#
#     out_imgpath = "/Users/arcturus/Desktop/MAS/FDL_go/simulation_environment/Application/images/visib.png"
#     source_blendpath = "/Users/arcturus/Desktop/MAS/FDL_go/moon-hermite-v3-2.blend"
#     blendPyScript = "/Users/arcturus/Desktop/MAS/FDL_go/simulation_environment/data_makers/visibility.py"
#     blendApp = "/Applications/Blender/blender.app"
#
# =============================================================================
    if out_img_path is None:
        out_img_path = "./images/visib.png"

    if input_blender_file_path is None:
        input_blender_file_path = "../../moon-hermite-v3-2.blend"

    if blender_pyscript_path is None:
        blender_pyscript_path = "../data_makers/visibility.py"

    if blender_app_path is None:
        blender_app_path = "/Applications/Blender/blender.app"

    if platform in ("linux", "linux2"):
        # Linux
        blender_app_path = "blender"
        input_blender_file_path = "./images/moon-hermite-v3-2.blend"
        cmd = ["blender", "-b", input_blender_file_path, "-P",
               blender_pyscript_path, "--", str(x_val), str(y_val),
               str(antenna_height), str(radius), out_img_path, "robot.json"]

    elif platform == "darwin":
        # macOS
        # Command line blender/python api command
        cmd = ["open", blender_app_path, "--args", "-b",
               input_blender_file_path, "-P", blender_pyscript_path, "--",
               str(x_val), str(y_val), str(antenna_height), str(radius),
               out_img_path, "robot.json"]

    elif platform == "win32":
    # Windows
        raise ValueError("Windows version not available")

    # cmd = ["/usr/bin/blender ", " -b images/moon-hermite-v3-2.blend -P ../data_makers/visibility.py -- 3.6 1.5 0.2 5. ./temporal.pngn robot.json"]
    # os.system(" /usr/bin/blender  -b /home/robolab/ExamplesFDL2018/simulation_environment/Application/images/moon-hermite-v3-2.blend -P ../data_makers/visibility.py -- 3.6 1.5 0.2 5. ./temporal.pngn robot.json")

    print("Attempting to render/write Blender IMG file to " + out_img_path)
    print("Platform: " + str(platform))
    print("command line call: " + str(cmd))

    # Call blender and write the image to disk
    call(cmd)
    return out_img_path


def comm_threshold(file_path, thresh, out_path=None, save=True):
    """ Something """
    im_file = imread(file_path)
    for i in range(len(im_file[0])):
        for j in range(len(im_file[:])):
            if im_file[j][i] <= thresh:
                im_file[j][i] = 0.0

    if out_path is None:
        out_path = file_path.split(".")
        out_path[-2] = out_path[-2] + "_thresh"
        out_path = ".".join(out_path)

    if save:
        plt.imsave(out_path, im_file, cmap='gray')

    return im_file


def has_signal(receiver_position, comm_map):
    """ Checks if commMap overlaps with receiverPos """
    x_ind, y_ind = receiver_position
    if comm_map[y_ind][x_ind] >= 0.0001:
        return True

    return False


def indices_available(thresh_comm_map):
    """
    Return indices that are non-zero and towards the edges of the bubble.

    Parameters
    ----------
    thresh_comm_map : np.ndarray
        an array of the pre-thresholded comm img

    """
    tcm = thresh_comm_map
    mean = np.mean(tcm[tcm != 0])
    std = np.std(tcm[tcm != 0])
    inds = []
    for i in range(len(tcm[0])):
        for j in range(len(tcm[:])):
            if tcm[j][i] > 0.001 and tcm[j][i] < (mean - std / 2.0):
                inds.append((i, j))

    return inds


def avail_indices_map(inds, shape):
    """
    Takes index tuples from indices_available and turns them into an
    img array.

    Parameters
    ----------
    inds : list of tuples
        Indexable list containing tuple of xy-coordinates such as
        [(x1,y1), (x2,y2),...].
    shape : tuple
        shape of the desired img array
    """
    z_val = np.zeros(shape)
    for ind in inds:
        x_val, y_val = ind
        z_val[y_val][x_val] = 255

    return z_val


def printxy(x_val, y_val, im1, im2):
    """ Something """
    # shared xy coords but different values
    print(im1[y_val][x_val])
    print(im2[y_val][x_val])

#from matplotlib import pyplot as plt
#plt.imshow(image3)


def main():
    """ Testing Comm Bubble code. """
    #image_path = "/Users/arcturus/Desktop/MAS/FDL_go/simulation_environment/Application/images/hermite.png"

    # get visibility | x,y,ant_height,radius
    rad = .01
    antenna_height = .05
    im2path = write_visibility(1.1, 0.0, antenna_height, rad)
    # grab DEM too, should be the same size as imread(im2path)
    #im3path = "/Users/arcturus/Desktop/MAS/FDL_go/simulation_environment/Application/images/hermite_a_dem_mosaic_1m_v3-255--1336x984.png"

    #image = imread(image_path)
    image2 = imread(im2path)
    #image3 = imread(im3path)

    #print("radius, antenna height: " + str((rad, antenna_height)) + "\nmax: " + str(np.max(image2)) + "\nstd: " + str(np.std(image2)) + "\nmean: "+ str(np.mean(image2)))
    #
    comm = comm_threshold(im2path, .111)
    inds = indices_available(comm)
    inds_map = avail_indices_map(inds, np.shape(comm))

    fig = plt.figure()
    pyax = fig.add_subplot(3, 1, 1)
    pyax.imshow(image2, aspect='equal')
    pyax = fig.add_subplot(3, 1, 2)
    pyax.imshow(comm, aspect='equal')
    pyax = fig.add_subplot(3, 1, 3)
    pyax.imshow(inds_map, aspect='equal')

    print('done')
    return comm


if __name__ == '__main__':
    main()
