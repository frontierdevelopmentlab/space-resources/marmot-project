/**
 *  Leaflet JS based visualization
 *
 *  View maps and their different layer of information
 *  And overlaid information such as path and markers.
 *
 **/


// --- custom function for xy standard coordinates
var yx = L.latLng;
var xy = function (x, y) {
  if (L.Util.isArray(x)) { // When doing xy([x, y]);
    return yx(x[1], x[0]);
  }
  return yx(y, x); // When doing xy(x, y);
};


// --- Preparing map tiles layers
var image_map_url = "assets/img/moon-surface.png";
var image_sci_url = "assets/img/moon-surface-sci.png";
var image_bounds = [ [0, 0], [984, 1336] ];

var maplayer_base = L.imageOverlay(image_map_url, image_bounds);
var maplayer_sci = L.imageOverlay(image_sci_url, image_bounds);


// --- Preparing map markers
var lander_pos = xy(175.2, 445.0);
var marker_lander = L.marker(lander_pos).bindPopup('175.2, 445.0 somewhere in the wild space'); 
var marker_poi = L.marker(xy(275.2, 645.0)).bindPopup("Science point of interest"); 
//var travel = L.polyline([lander_pos, lander_pos2]);//.addTo(map);
var markers_all = L.layerGroup([marker_lander, marker_poi]);


// --- Creating main map object
var map = L.map('mapid', {
    crs: L.CRS.Simple,
    minZoom: -5,
	layers: [maplayer_base, maplayer_sci]
});
map.fitBounds(image_bounds);
map.setView([400, 400], 1);


// --- Defining markers in the form [y, x]

// Configuration of a layer groups set-up
//  baseMaps display the base map and a path 
var baseMaps = {
    "Moon": maplayer_base,
    "Science map": maplayer_sci
	// Add layers here
	// "slope": slopePNG
};

var overlayMaps = {
    "All markers": markers_all
};

L.control.layers(baseMaps, overlayMaps).addTo(map);



// ------------ GeoJSON
var myStyle = {
  "color": "#ff7800",
  "weight": 5,
  "opacity": 0.65
};

// --- This parts loads a GeoJSON from geojson_data

//var geojson_layer = L.geoJSON(robots_paths);

let xhr = new XMLHttpRequest();
xhr.open('GET', 'http://127.0.0.1/robots_paths.geojson');
xhr.setRequestHeader('Content-Type', 'application/json');
xhr.onload = function() {
    if (xhr.status === 200) {
		console.log("Adding geojson to the map");
        L.geoJSON(JSON.parse(xhr.responseText)).addTo(map);
    }
};
xhr.send();

//var geojson_data = ""; // persistent variable
//var xmlhttp = new XMLHttpRequest();
//xmlhttp.onreadystatechange = function () {
//  if (this.readyState == 4 && this.status == 200) {
//    geojson_data = JSON.parse(this.responseText);
//	//console.log("Loading geojson "+this.responseText);
//	L.geoJSON(geojson_data, {
//		style: function (feature) {
//			return {color: feature.properties.color};
//		}
//	}).bindPopup(function (layer) {
//		return layer.feature.properties.description;
//	}).addTo(map);
//  }
//};
//xmlhttp.open("GET", "robots_paths.geojson", true);
//xmlhttp.send();

// Using an external library
//var geojson_layer = L.GeoJSON.AJAX("robots_paths.geojson");
//geojson_layer.addTo(map);

//        {
//            "type": "Feature",
//            "geometry": {
//                "type": "LineString",
//                "coordinates": [
//                    [-104.99820470809937, 39.74979664004068],
//                    [-104.98689651489258, 39.741052354709055]
//                ]
//            },
//            "properties": {
//                "popupContent": "This is a free bus line that will take you across downtown.",
//                "underConstruction": false
//            },
//            "id": 3
//        }

// --------------------------------------------------------------- Memos:

//   var geojson_path = [{
//     "type": "LineString",
//     "coordinates": [[20, 150], [20, 149], [20, 148], [20, 147], [20, 146],
//                     [20, 145], [20, 144], [20, 143], [20, 142], [20, 141], [20, 140], [20, 139],
//                     [20, 138], [20, 137], [20, 136], [20, 135], [20, 134], [20, 133], [20, 132],
//                     [20, 131], [20, 130], [21, 130], [22, 130], [23, 130], [24, 130], [25, 130],
//                     [26, 130], [27, 130], [27, 129], [27, 128], [28, 128], [29, 128], [30, 128],
//                     [31, 128], [32, 128], [33, 128], [34, 128], [35, 128], [36, 128], [37, 128]]
//   }, {
//     "type": "LineString",
//     "coordinates": [[105, 40], [110, 345], [115, 355]]
//   }];
//   L.geoJSON(geojson_path, {
//     style: myStyle
//   }).addTo(map);
//
// loading GeoJSON file - Here my html and usa_adm.geojson file resides in same folder
//     $.getJSON("apolo.geojson",function(data){
//     // L.geoJson function is used to parse geojson file and load on to map
//     L.geoJson(data).addTo(map);
//     });

//     $.getJSON("usa_adm.geojson",function(data){
// // add GeoJSON layer to the map once the file is loaded
//     var datalayer = L.geoJson(data ,{
//     onEachFeature: function(feature, featureLayer) {
//     featureLayer.bindPopup(feature.properties.NAME_1);
//     }
//     }).addTo(newMap);
//     newMap.fitBounds(datalayer.getBounds());
//     });
//

