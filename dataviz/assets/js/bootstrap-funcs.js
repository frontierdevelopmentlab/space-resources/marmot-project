function robot_button_toggle() {
    if ($("#rbutton1").hasClass("active")) {
        $("#rbutton1").button("toggle");
    }

    if ($("#rbutton2").hasClass("active")) {
        $("#rbutton2").button("toggle");
    }

    if ($("#rbutton3").hasClass("active")) {
        $("#rbutton3").button("toggle");
    }

    $(this).button('toggle');
}

function layer_button_toggle() {
    $(this).button('toggle');
}

function load_geojson() {};

$(document).ready(function() {
    /* $(".btn-primary").click(button_toggle);
    $(".btn-secondary").click(button_toggle);
    $(".btn-success").click(button_toggle); */
    /* $("#lbutton1").active() */
    /* $("#rbutton1").on('click', button_toggle);
    $("#rbutton2").on('click', button_toggle);
    $("#rbutton3").on('click', button_toggle);

    $("#lbutton1").on('click', button_toggle);
    $("#lbutton2").on('click', button_toggle);
    $("#lbutton3").on('click', button_toggle); */
});

$("#rbutton1").on('click', robot_button_toggle);
$("#rbutton2").on('click', robot_button_toggle);
$("#rbutton3").on('click', robot_button_toggle);

$("#lbutton1").on('click', layer_button_toggle);
$("#lbutton2").on('click', layer_button_toggle);
$("#lbutton3").on('click', layer_button_toggle);
