# marmot-project

## Multi-Agent Resource Mission Operations Tools

An FDL 2018 Space Resources project allowing for multi-agent robotic extraterrestrial mission planning operations.

## Install (ubuntu like systems)

Currently, the only official supported platforms are Ubuntu 16.04 to 18.04 on Python 3.5 and higher.

    NOTE: MARMOT can still work on other unix-like systems if you are able to install all the packages in the following section.

To begin, install the following dependency programs:

```bash
~$ sudo apt update && sudo apt upgrade
~$ sudo apt install python3-pip python3-numpy python3-scipy python3-matplotlib python3-networkx python3-opencv python3-geojson
```

Next, we will need to install the GDAL stack which is located in a special ppa for Ubuntu called `ubuntugis`:

```bash
~$ sudo add-apt-repository -y ppa:ubuntugis/ppa
~$ sudo apt update && sudo apt upgrade
~$ sudo apt -y install gdal-bin python3-gdal libgdal-dev
```

Finally, install MARMOT through `pip`. `graphery` and `mapstery` should install automatically because they are dependencies.

```bash
~$ python3 -m pip install marmot-project
```

## Usage

The `examples` folder provides a couple Jupyter notebooks with explanation. Please refer to those or drop an issue if you have any problems!

## License

This project uses the **BSD 3-Clause License**.

Copyright (c) 2019, Frontier Development Lab / Space Resources

If you have any questions, please contact the authors of the project.
