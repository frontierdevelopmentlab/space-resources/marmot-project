"""
MARMOT Project setuptools setup.

Created thanks to:
    https://packaging.python.org/en/latest/distributing.html
    https://github.com/pypa/sampleproject

"""
import codecs
from os import path
from setuptools import setup, find_packages

HERE = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with codecs.open(path.join(HERE, 'README.md'), encoding='utf-8') as f:
    LONG_DESCRIPTION = f.read()

setup(
    version="0.0.1",
    name="marmot-project",
    description="Multi-Agent Resource Missions Operations Tool",
    long_description=LONG_DESCRIPTION,
    url="https://gitlab.com/frontierdevelopmentlab/space-resources/marmot-project",
    license="BSD",
    author="Red Boumghar, Francisco Rodriguez Lera, Zahi Kakish, Drew Bischel, Ana Mosquera",
    install_requires=["mapstery", "graphery", "geojson", "numpy"],
    python_requires='>=3',
    extras_require={
        "test": ["pytest", "pytest-cov"]
    },
    packages=find_packages(exclude=["tests", "docs", "viewers"]),
    keywords="information, path planning, route planning, communication, resources, prospection",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License"
    ],
)
